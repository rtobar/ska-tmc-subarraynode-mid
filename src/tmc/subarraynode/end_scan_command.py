"""
EndScan Command class for SubarrayNode.
"""

# Third party imports
# Tango imports
import tango
from tango import DevFailed

# Additional import
from tmc.subarraynode import const
from ska.base.commands import BaseCommand
from tmc.common.tango_client import TangoClient
from tmc.common.tango_server_helper import TangoServerHelper
from tmc.subarraynode.device_data import DeviceData


class EndScan(BaseCommand):
    """
    A class for SubarrayNode's EndScan() command.

    Ends the scan. It is invoked on subarray after completion of the scan duration. It can
    also be invoked by an external client while a scan is in progress, Which stops the scan
    immediately irrespective of the provided scan duration.

    """

    def do(self):
        """
        Method to invoke Endscan command.

        return:
            None

        raises:
            DevFailed if the command execution is not successful.
        """
        self.this_server = TangoServerHelper.get_instance()
        device_data = DeviceData.get_instance()
        device_data.is_release_resources_command_executed = False
        device_data.is_restart_command_executed = False
        device_data.is_abort_command_executed = False
        device_data.is_obsreset_command_executed = False
        try:
            if device_data.scan_timer_handler.is_scan_running():
                device_data.scan_timer_handler.stop_scan_timer()
            device_data.is_scan_running = False
            device_data.is_scan_completed = True

            self.endscan_sdp(device_data)
            self.endscan_csp(device_data)
            device_data._scan_id = ""
            # TODO: For Future Use
            # check whether csp and sdp are in idle ObsState and if dishes are assigned calculate ObsState.
            self.this_server.set_status(const.STR_SCAN_COMPLETE)
            self.logger.info(const.STR_SCAN_COMPLETE)
            self.this_server.write_attr("activityMessage", const.STR_END_SCAN_SUCCESS, False)

        except DevFailed as dev_failed:
            log_msg = f"{const.ERR_END_SCAN_CMD_ON_GROUP}{dev_failed}"
            self.logger.exception(dev_failed)
            tango.Except.throw_exception(
                const.STR_END_SCAN_EXEC,
                log_msg,
                "SubarrayNode.EndScan",
                tango.ErrSeverity.ERR,
            )

    def endscan_sdp(self, device_data):
        """
        EndScan command on SDP Subarray Leaf Node
        """
        this_server = TangoServerHelper.get_instance()
        sdp_client = TangoClient(this_server.read_property("SdpSubarrayLNFQDN")[0])
        sdp_client.send_command(const.CMD_END_SCAN)
        self.logger.debug(const.STR_SDP_END_SCAN_INIT)
        self.this_server.write_attr("activityMessage", const.STR_SDP_END_SCAN_INIT, False)

    def endscan_csp(self, device_data):
        """
        EndScan command on CSP Subarray Leaf Node
        """
        this_server = TangoServerHelper.get_instance()
        csp_client = TangoClient(this_server.read_property("CspSubarrayLNFQDN")[0])
        csp_client.send_command(const.CMD_END_SCAN)
        self.logger.debug(const.STR_CSP_END_SCAN_INIT)
        self.this_server.write_attr("activityMessage", const.STR_CSP_END_SCAN_INIT, False)
