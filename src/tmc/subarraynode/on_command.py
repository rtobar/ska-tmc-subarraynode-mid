"""
On Command class for SubarrayNode
"""

# Third party imports
# Tango imports
import tango
from tango import DevFailed

# Additional import
from tmc.subarraynode import const
from ska.base.commands import ResultCode
from ska.base import SKASubarray

from tmc.common.tango_client import TangoClient
from tmc.subarraynode.health_state_aggregator import HealthStateAggregator
from tmc.subarraynode.obs_state_aggregator import ObsStateAggregator
from tmc.subarraynode.receive_addresses import ReceiveAddressesUpdater
from tmc.common.tango_server_helper import TangoServerHelper


class On(SKASubarray.OnCommand):
    """
    A class for the SubarrayNode's On() command.

    This command invokes On Command on CSPSubarray and SDPSubarray through respective leaf nodes. This comamnd
    changes Subaray device state from OFF to ON.

    """

    def do(self):
        """
        Method to invoke On command.

        return:
            A tuple containing a return code and a string message indicating status. The message is for
            information purpose only.

        rtype:
            (ResultCode, str)

        raises:
            DevFailed if the command execution is not successful

        """
        device_data = self.target
        self.this_server = TangoServerHelper.get_instance()
        device_data.is_restart_command_executed = False
        device_data.is_release_resources_command_executed = False
        device_data.is_abort_command_executed = False
        device_data.is_obsreset_command_executed = False
        self.set_csp_client(device_data)
        self.set_sdp_client(device_data)

        message = "On command completed OK"
        self.logger.info(message)
        return (ResultCode.OK, message)

    def set_csp_client(self, device_data):
        """
        set up csp devices
        """
        # Create proxy for CSP Subarray Leaf Node
        this_server = TangoServerHelper.get_instance()
        csp_subarray_ln_fqdn = this_server.read_property("CspSubarrayLNFQDN")[0]
        log_msg = f"{const.STR_SA_PROXY_INIT}{csp_subarray_ln_fqdn}"
        csp_subarray_ln_client = TangoClient(
            this_server.read_property("CspSubarrayLNFQDN")[0]
        )
        self.logger.info(log_msg)
        self.turn_on_leaf_node(csp_subarray_ln_client)

    def set_sdp_client(self, device_data):
        """
        set up sdp devices
        """
        # Create proxy for SDP Subarray Leaf Node
        this_server = TangoServerHelper.get_instance()
        sdp_subarray_ln_fqdn = this_server.read_property("SdpSubarrayLNFQDN")[0]
        log_msg = f"{const.STR_SA_PROXY_INIT}{sdp_subarray_ln_fqdn}"
        sdp_subarray_ln_client = TangoClient(
            this_server.read_property("SdpSubarrayLNFQDN")[0]
        )
        self.logger.info(log_msg)
        self.turn_on_leaf_node(sdp_subarray_ln_client)

    def turn_on_leaf_node(self, tango_client):
        # Invoke ON command on lower level devices
        try:
            tango_client.send_command(const.CMD_ON)
        except DevFailed as dev_failed:
            log_msg = f"{const.ERR_INVOKING_ON_CMD}{dev_failed}"
            self.logger.exception(log_msg)
            self.this_server.write_attr("activityMessage", log_msg, False)

            tango.Except.throw_exception(
                dev_failed[0].desc,
                const.ERR_INVOKE_ON_CMD_ON_SA,
                "SubarrayNode.On()",
                tango.ErrSeverity.ERR,
            )
