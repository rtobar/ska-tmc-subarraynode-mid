"""
Scan Command class for SubarrayNode
"""

# Third party imports
# Tango imports
import tango
from tango import DevFailed

# Additional import
from ska.base.commands import ResultCode
from ska.base import SKASubarray

from tmc.common.tango_server_helper import TangoServerHelper
from tmc.common.tango_client import TangoClient
from tmc.subarraynode import const
from tmc.subarraynode.device_data import DeviceData


class Scan(SKASubarray.ScanCommand):
    """
    A class for SubarrayNode's Scan() command.

    The command accepts Scan id as an input and executes a scan on the subarray.
    Scan command is invoked on respective CSP and SDP subarray node for the provided
    interval of time. It checks whether the scan is already in progress.
    If yes it throws error showing duplication of command.

    """

    def do(self, argin):
        """
        Method to invoke Scan command.

        :param argin: DevString. JSON string containing id.

        :Example:   {"id": 1}

        Note: Above JSON string can be used as an input argument while invoking this command from JIVE.

        return:
            A tuple containing a return code and a string message indicating status.
            The message is for information purpose only.

        rtype:
            (ReturnCode, str)

        raises:
            DevFailed if the command execution is not successful
        """
        device_data = DeviceData.get_instance()
        device_data.is_scan_completed = False
        device_data.is_release_resources_command_executed = False
        device_data.is_restart_command_executed = False
        device_data.is_abort_command_executed = False
        device_data.is_obsreset_command_executed = False
        self.this_server = TangoServerHelper.get_instance()
        try:
            log_msg = f"{const.STR_SCAN_IP_ARG}{argin}"
            self.logger.debug(log_msg)
            self.this_server.write_attr("activityMessage", log_msg, False)
            device_data.is_scan_running = True
            self.scan_sdp(device_data, argin)
            self.scan_csp(device_data, argin)
            # TODO: Update observation state aggregation logic
            # if self._csp_sa_obs_state == ObsState.IDLE and self._sdp_sa_obs_state ==\
            #         ObsState.IDLE:
            #     if len(self.dish_pointing_state_map.values()) != 0:
            #         self.calculate_observation_state()

            # Set timer to invoke EndScan command after scan duration is complete.
            self.logger.info("Setting scan timer")
            device_data.scan_timer_handler.start_scan_timer(device_data.scan_duration)
            self.this_server.set_status(const.STR_SA_SCANNING)
            self.logger.info(const.STR_SA_SCANNING)
            self.this_server.write_attr("activityMessage", const.STR_SCAN_SUCCESS, False)

            return (ResultCode.STARTED, const.STR_SCAN_SUCCESS)
        except DevFailed as dev_failed:
            log_msg = f"{const.ERR_SCAN_CMD}{dev_failed}"
            self.logger.exception(dev_failed)
            tango.Except.throw_exception(
                const.STR_SCAN_EXEC, log_msg, "SubarrayNode.Scan", tango.ErrSeverity.ERR
            )

    def scan_sdp(self, device_data, argin):
        """
        set up sdp devices
        """
        # Invoke scan command on Sdp Subarray Leaf Node with input argument as scan id
        this_server = TangoServerHelper.get_instance()
        sdp_client = TangoClient(this_server.read_property("SdpSubarrayLNFQDN")[0])
        sdp_client.send_command(const.CMD_SCAN, argin)
        self.logger.info(const.STR_SDP_SCAN_INIT)
        self.this_server.write_attr("activityMessage", const.STR_SDP_SCAN_INIT, False)

    def scan_csp(self, device_data, argin):
        """
        set up csp devices
        """
        # Invoke Scan command on CSP Subarray Leaf Node
        csp_argin = [argin]
        this_server = TangoServerHelper.get_instance()
        csp_client = TangoClient(this_server.read_property("CspSubarrayLNFQDN")[0])
        csp_client.send_command(const.CMD_START_SCAN, csp_argin)
        self.logger.info(const.STR_CSP_SCAN_INIT)
        self.this_server.write_attr("activityMessage", const.STR_CSP_SCAN_INIT, False)
