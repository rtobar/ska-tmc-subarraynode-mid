# -*- coding: utf-8 -*-
#
# This file is part of the SubarrayNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """subarraynode"""
version = "0.1.5"
version_info = version.split(".")
description = """Subarray Node is a coordinator of the complete M&C system."""
author = "Team NCRA"
author_email = "telmgt-internal@googlegroups.com"
license = """BSD-3-Clause"""
url = """https://www.skatelescope.org"""
copyright = """"""
